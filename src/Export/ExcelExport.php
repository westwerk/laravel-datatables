<?php

namespace Westwerk\DataTables\Export;


use Illuminate\Http\Response;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Westwerk\DataTables\Export\Excel\Sheet;
use Westwerk\DataTables\Http\Requests\RequestInterface;

class ExcelExport
{

    /**
     * @var null|Spreadsheet
     */
    protected $spreadsheet = null;

    /**
     * @var Sheet[]
     */
    protected $sheets = [];

    /**
     * @var null|string
     */
    protected $filename = null;

    /**
     * ExcelExport constructor.
     *
     * @param null $filename
     */
    public function __construct($filename = null)
    {
        $this->filename = $filename;
        $this->spreadsheet = new Spreadsheet();
    }

    /**
     * @param RequestInterface $datatable
     * @param null $name
     */
    public function addSheet(RequestInterface $datatable, $name = null)
    {
        $sheet = $this->spreadsheet->getSheet(count($this->sheets));
        $this->sheets[] = new Sheet($sheet, $datatable, $name);
    }


    public function response()
    {

        set_time_limit(0);

        $this->compile();

        $filename = $this->getFilename();
        if ($filename === null) {
            $filename = md5(uniqid()) . '.xlsx';
        }

        $response = new Response();
        $response->withHeaders([
            'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
            'Content-Description' => 'Datatables Export',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            'Pragma' => 'no-cache',
            'Expires' => '0',
        ]);

        $writer = new Xlsx($this->spreadsheet);

        ob_start();
        $writer->save('php://output');
        $response->setContent(ob_get_clean());

        return $response;
    }


    protected function compile()
    {
        // Compile sheets
        foreach ($this->sheets as $sheet) {
            $sheet->compile();
        }

        // Text wrap
        $this->spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
    }

    /**
     * @return null
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param null $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

}