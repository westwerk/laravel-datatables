<?php

namespace Westwerk\DataTables\Export\Excel;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Westwerk\DataTables\Http\Requests\DataTableRequest;
use Westwerk\DataTables\Http\Requests\RequestInterface;

class Sheet
{

    /**
     * @var RequestInterface
     */
    protected $datatable = null;

    /**
     * @var null|string
     */
    protected $name = null;

    /**
     * @var null|Worksheet
     */
    protected $sheet = null;

    /**
     * Sheet constructor.
     * @param Worksheet $sheet
     * @param RequestInterface $datatable
     * @param null $name
     */
    public function __construct(Worksheet $sheet, RequestInterface $datatable, $name = null)
    {
        $this->sheet     = $sheet;
        $this->datatable = $datatable;
        $this->name      = $name;

        if ($name !== null) {
            $sheet->setTitle($name);
        }
    }


    public function compile()
    {
        $datatable = $this->getDatatable();
        $sheet     = $this->getSheet();

        $columns  = $datatable->getExportColumns();
        $colCount = 0;
        foreach ($columns as $colNo => $column) {
            if (!!array_get($column, 'export', true)) {
                $colCount++;
                $cell = $sheet->getCellByColumnAndRow($colNo, 1, true);
                $cell->setValue(array_get($column, 'props.title', ''));
                $cell->getStyle()->getFont()->setBold(true);
                $cell->getStyle()->getBorders()->getBottom()->setBorderStyle(true);
            }
        }
        $sheet->freezePane('A2');
        $rowOffset = 2;


        $parseValue = function ($value) {
            if ($value === null)
                return '';

            if (is_array($value)) {
                if (count($value) > 1) {
                    //$sheet->getStyleByColumnAndRow($colNo, $rowNo)->getAlignment()->setWrapText(true);
                }
                $value = array_reduce($value, function ($carry, $item) use (&$types) {
                    if (strlen($carry) > 0) {
                        $carry .= "\n";
                    }
                    return $carry . $item;
                });
            }
            return $value;
        };

        $datatable->getExportData(function ($rowNo, $colNo, $row, $column, $value, RequestInterface $request) use (
            $parseValue,
            $rowOffset,
            $sheet
        ) {
            if (!!array_get($column, 'export', true)) {
                $rowNo = $rowNo + $rowOffset;
                $type  = $this->getCellDataType($value, array_get($column, 'type', null));
                $value = $parseValue($value);
                try {
                    $sheet->setCellValueExplicitByColumnAndRow($colNo, $rowNo, $value, $type);
                }
                catch (\Exception $e) {
                    $sheet->setCellValueByColumnAndRow($colNo, $rowNo, '#VALUE!');
                }
            }
        });

        // Auto size
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(true);
        /** @var Cell $cell */
        foreach ($cellIterator as $cell) {
            $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
        }

        // Autofilter
        $sheet->setAutoFilterByColumnAndRow(0, 1, $colCount - 1, 1);

    }

    /**
     * @return RequestInterface
     */
    protected function getDatatable()
    {
        return $this->datatable;
    }

    /**
     * @param RequestInterface $datatable
     */
    protected function setDatatable($datatable)
    {
        $this->datatable = $datatable;
    }

    /**
     * @return null|string
     */
    protected function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    protected function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|Worksheet
     */
    protected function getSheet()
    {
        return $this->sheet;
    }

    /**
     * @param null|Worksheet $sheet
     */
    protected function setSheet($sheet)
    {
        $this->sheet = $sheet;
    }

    /**
     * @param $value
     * @param $type
     *
     * @return string
     */
    protected function getCellDataType($value, $type)
    {
        $phpExcelType = DataType::TYPE_STRING;
        switch ($type) {
            case DataTableRequest::VALUE_TYPE_INTEGER:
            case DataTableRequest::VALUE_TYPE_FLOAT:
            case DataTableRequest::VALUE_TYPE_BOOLEAN:
                $phpExcelType = DataType::TYPE_NUMERIC;
                break;

            case DataTableRequest::VALUE_TYPE_STRING:
            case DataTableRequest::VALUE_TYPE_DATE:
            case DataTableRequest::VALUE_TYPE_TIME:
            case DataTableRequest::VALUE_TYPE_DATETIME:
                $phpExcelType = DataType::TYPE_STRING;
                break;

            default:
                if ($type === null && is_numeric($value)) {
                    $phpExcelType = DataType::TYPE_NUMERIC;
                }
        }
        return $phpExcelType;
    }


}