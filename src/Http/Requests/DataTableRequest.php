<?php

namespace Westwerk\DataTables\Http\Requests;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;
use Illuminate\View\View;
use Illuminate\Support\Arr;
use Westwerk\DataTables\Export\ExcelExport;

/**
 * Class Request
 *
 * @package Takeback\Http\Requests\Api\DataTables
 */
abstract class DataTableRequest extends FormRequest implements RequestInterface
{
    const REQUEST_TYPE_COLUMNS = 'columns';
    const REQUEST_TYPE_DATA = 'data';
    const REQUEST_TYPE_EXPORT = 'export';

    const FILTER_VALUE_NULL_FILTER = '__NULL__';

    const VALUE_TYPE_STRING = 'string';
    const VALUE_TYPE_INTEGER = 'integer';
    const VALUE_TYPE_FLOAT = 'float';
    const VALUE_TYPE_DATE = 'date';
    const VALUE_TYPE_TIME = 'time';
    const VALUE_TYPE_DATETIME = 'datetime';
    const VALUE_TYPE_BOOLEAN = 'boolean';
    const VALUE_TYPE_ACTIONS = 'actions';

    /**
     * @var array
     */
    protected $columns = null;

    /**
     * @var null|string
     */
    protected $requestType = null;

    /**
     * @var null|string
     */
    protected $countColumn = null;

    /**
     * @var null|Builder
     */
    private $queryBuilder = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'length' => 'integer|max:1000',
            'start' => 'integer',
            'search.value' => 'string',
            'columns' => 'array',
            'order' => 'array',
            'order.*.column' => 'required|integer',
            'order.*.dir' => 'required|in:asc,desc',
        ];
    }

    /**
     * @param Factory $factory
     *
     * @return mixed
     */
    public function validator(Factory $factory)
    {
        return $factory->make($this->all(), $this->rules());
    }

    /**
     * @param Guard $auth
     *
     * @return bool
     */
    public function authorize(Guard $auth)
    {
        return app('auth')->check();
    }

    /**
     * @return array
     */
    abstract function setupColumns();

    /**
     * @param Builder $qb
     */
    abstract function setupQueryBuilder(Builder $qb);

    /**
     * @param Builder $qb
     *
     * @return array
     */
    public function handleDataRequest(Builder $qb)
    {
        // Set internals
        $this->setRequestType(self::REQUEST_TYPE_DATA);
        $this->setQueryBuilder($qb);

        // Setup query builder in concrete class
        $this->setupQueryBuilder($qb);

        // Total count
        $total = $this->count($qb);

        // Apply filters
        $this->applyFilters($qb);

        // Number of filtered records
        $filtered = $this->count($qb);

        // Apply sorting
        $this->applySorting($qb);

        // Apply limits
        $this->applyLimits($qb);

        // Fetch data
        $data = $this->getData($qb);

        return [
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => $data,
        ];
    }

    /**
     * @return array
     */
    public function handleColumnsRequest()
    {
        // Set internals
        $this->setRequestType(self::REQUEST_TYPE_COLUMNS);
        $columns = array_map(function ($column) {
            return Arr::only($column['props'], ['alias', 'title', 'sortable', 'filter', 'type']);
        }, $this->getColumnsBasedOnRequest());

        $actions = $this->getActions();

        return compact('columns', 'actions');
    }

    /**
     * @param Builder $qb
     * @param null $filename
     *
     * @return \Illuminate\Http\Response
     */
    public function handleExportRequest(Builder $qb, $filename = null)
    {
        // Set internals
        $this->setRequestType(self::REQUEST_TYPE_EXPORT);
        $this->setQueryBuilder($qb);

        // Setup query builder in concrete class
        $this->setupQueryBuilder($qb);

        // Apply filters
        $this->applyFilters($qb);

        // Apply sorting
        $this->applySorting($qb);

        $export = new ExcelExport($filename);
        $export->addSheet($this);

        return $export->response();
    }

    /**
     * @param Builder $qb
     */
    protected function applyFilters(Builder $qb)
    {
        $rcolumns = $this->input('columns');
        $columns = $this->getDisplayColumns();
        if ($rcolumns) {
            // Global filter
            $global = trim($this->input('search.value'));
            if (!empty($global)) {
                $qb->where(function (Builder $query) use ($rcolumns, $global, $columns) {
                    foreach ($columns as $column) {
                        if (!!Arr::get($column, 'props.filter.global', true)
                            && in_array(Arr::get($column, 'props.filter.type'), ['text'])
                        ) {
                            $query->orWhere(function ($subquery) use ($column, $global) {
                                $this->applyFilter($subquery, $column, $global, true);
                            });
                        }
                    }
                });
            }

            //Column filters
            foreach ($rcolumns as $index => $rcolumn) {
                $value = trim(Arr::get($rcolumn, 'search.value', ''));
                if (strlen($value)) {
                    if (($column = $columns[ $index ])) {
                        if (isset($column['props']['filter'])) {
                            $qb->where(function ($query) use ($column, $value) {
                                $this->applyFilter($query, $column, $value);
                            });
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Builder $query
     * @param         $column
     * @param         $value
     */
    protected function applyFilter(Builder $query, $column, $value, $global = false)
    {
        if (array_key_exists('filter', $column)) {
            if (is_callable($column['filter'])) {
                call_user_func($column['filter'], $query, $column, $value, $global);

            } else {
                if ($value === self::FILTER_VALUE_NULL_FILTER) {
                    $query->whereNull($column['filter']);
                } elseif (($type = Arr::get($column, 'props.filter.type', false)) !== false) {
                    switch ($type) {
                        case 'text':
                            $query->where($column['filter'], 'like', '%' . $value . '%');
                            break;
                        case 'date':
                            $query->where(\DB::raw('DATE_FORMAT('
                                . $column['filter'] . ', \''
                                . Arr::get($column, 'props.filter.props.format', '%y.%d.%Y')
                                . '\')'), 'like', '%' . $value . '%');
                            break;
                        case 'daterange':
                            //$value contains #-separated unix timestamps for start and end date
                            if (strpos($value, '#') !== false) {
                                $dates = explode('#', $value);
                                $dates = array_map(function ($item) {
                                    return 0.001 * $item;
                                }, $dates);
                                $query->whereBetween(\DB::raw('UNIX_TIMESTAMP(' . $column['filter'] . ')'), $dates);
                            }
                            break;

                        case 'bool':
                            if ($value === 'true') {
                                $query->where($column['filter'], '=', 1);
                            }
                            break;
                        case 'select':
                            $values = explode(',', $value);
                            $mapping = Arr::only($column['options'], $values);
                            $mapping = array_filter($mapping, function ($item) {
                                return !!$item;
                            });
                            if (count($mapping)) {
                                $query->whereIn($column['filter'], $mapping);
                            }
                            break;
                        case 'state':
                            if (in_array($value, $column['options'])) {
                                $query->where($column['filter'], '=', $value);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        } else {
            throw new \InvalidArgumentException('No filter handler defined in DataTable column definition.');
        }
    }

    /**
     * @param Builder $qb
     */
    protected function applySorting(Builder $qb)
    {
        $orders = $this->input('order');
        if ($orders) {
            $columns = $this->getDisplayColumns();
            foreach ($orders as $order) {
                if (($column = $columns[ $order['column'] ])) {
                    if (!!Arr::get($column, 'props.sortable', false)) {
                        if (isset($column['sort'])) {
                            if (is_callable($column['sort'])) {
                                call_user_func($column['sort'], $qb, $order['dir']);
                            } elseif (is_string($column['sort'])) {
                                $qb->orderBy($column['sort'], $order['dir']);
                            }
                        } else {
                            throw new \InvalidArgumentException('No sort handler defined in DataTable column definition.');
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Builder $qb
     */
    protected function applyLimits(Builder $qb)
    {
        $start = (int)$this->input('start');
        $length = (int)$this->input('length');
        // length can be -1 meaning no limits
        if ($length !== -1) {
            $qb->skip($start)->take($length);
        }
    }

    /**
     * @param Builder $qb
     *
     * @return $this
     */
    protected function getData(Builder $qb)
    {
        return $qb->get()->map(function ($row) {
            return array_map(function ($column) use ($row) {
                return $this->getValue($column, $row);
            }, $this->getColumnsBasedOnRequest());
        });
    }

    /**
     * @param \Closure $callback
     *
     * @return void
     */
    public function getExportData(\Closure $callback)
    {
        $this->getQueryBuilder()->get()->each(function ($row, $rowNo) use ($callback) {
            $colNo = 0;
            foreach ($this->getExportColumns() as $column) {
                $value = $this->getValue($column, $row);
                $callback($rowNo, $colNo++, $row, $column, $value, $this);
            }
        });
    }

    /**
     * @param $column
     * @param $row
     *
     * @return string
     */
    protected function getValue($column, $row)
    {
        $valueKey = 'value';
        if ($this->getRequestType() === self::REQUEST_TYPE_EXPORT) {
            if (array_key_exists('exportValue', $column)) {
                $valueKey = 'exportValue';
            }
        }
        if (isset($column[ $valueKey ])) {
            $funcOrProp = $column[ $valueKey ];
            $modifiers = null;
            if (is_array($funcOrProp) && !is_callable($funcOrProp)) {
                $modifiers = array_slice($funcOrProp, 1);
                $funcOrProp = reset($funcOrProp);
            }
            if (is_callable($funcOrProp)) {
                // Determine value using a callback function
                $value = call_user_func($funcOrProp, $row);
                $value = is_scalar($value) ? (string)$value : $value;
            } else {
                // Assuming, that value is the name of a property
                $value = $row->$funcOrProp;
                if ($modifiers === null) {
                    $modifiers = ['trans']; // behaved like this before
                }
            }
            if ($value !== null) {
                foreach ($modifiers ?: [] as $m) {
                    $value = $m($value);
                }
            }
            if ($value instanceof View) {
                return $value->render();
            }
            return $value;
        } else {
            throw new \InvalidArgumentException('No value handler defined in DataTable column definition.');
        }
    }

    /**
     * @param Builder $qb
     *
     * @return mixed
     */
    protected function count(Builder $qb)
    {
        try {
            $qb = clone $qb;
            $qb->getQuery()->groups = null;
            $countColumn = $this->countColumn ?: $qb->getQuery()->from . '.id';
            $count = $qb->count(\DB::raw('DISTINCT ' . $countColumn));
        } catch (\Exception $e) {
            app('log')->error('Could not count datatable records in \'' .
                get_called_class() . '\' (countColumn: \'' . $countColumn . '\'): ' . $e->getMessage());

            return 0;
        }

        return $count;
    }

    /**
     * @return null|string
     */
    protected function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param null|string $requestType
     */
    protected function setRequestType($requestType)
    {
        $this->requestType = $requestType;
    }

    /**
     * @return array
     */
    protected function getColumns()
    {
        if ($this->columns === null) {
            $this->columns = $this->setupColumns();
        }

        return $this->columns;
    }

    /**
     * @return Builder|null
     */
    protected function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * @param Builder|null $queryBuilder
     */
    protected function setQueryBuilder($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return array
     */
    protected function getColumnsBasedOnRequest()
    {
        if ($this->getRequestType() === self::REQUEST_TYPE_EXPORT) {
            return $this->getExportColumns();
        } else {
            return $this->getDisplayColumns();
        }
    }

    /**
     * @return array
     */
    public function getDisplayColumns()
    {
        return array_values(array_filter($this->getAliasedColumns(), function ($column) {
            return !!Arr::get($column, 'visible', true);
        }));
    }

    /**
     * @return array
     */
    public function getExportColumns()
    {
        return array_values(array_filter($this->getAliasedColumns(), function ($column) {
            return !!Arr::get($column, 'exportable', true);
        }));
    }

    /**
     * @return array
     */
    abstract public function setupActions();

    /**
     * @return array
     */
    public function getActions()
    {
        $defs = $this->setupActions();

        return array_map(function ($def) {
            return \Illuminate\Support\Arr::except($def, ['handler', 'provider']);
        }, $defs);
    }

    /**
     * @return array
     */
    protected function getAliasedColumns()
    {
        return array_map(function ($column, $key) {
            if (!isset($column['props']['alias'])) {
                $column['props']['alias'] = $key;
            }

            return $column;
        }, $c = $this->getColumns(), array_keys($c));
    }

}
