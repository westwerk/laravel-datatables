<?php

namespace Westwerk\DataTables\Http\Requests;

use Illuminate\Database\Eloquent\Builder;


/**
 * Interface RequestInterface
 *
 * @package Westwerk\DataTables\Http\Requests
 */
interface RequestInterface
{

    /**
     * @return array
     */
    function setupColumns();

    /**
     * @return array
     */
    function getExportColumns();

    /**
     * @return array
     */
    function getDisplayColumns();

    /**
     * @param Builder $qb
     */
    function setupQueryBuilder(Builder $qb);

    /**
     * @param \Closure $callback
     *
     * @return mixed
     */
    function getExportData(\Closure $callback);
}