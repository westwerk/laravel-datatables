# Westwerk DataTables API helper

Version 1.3.0

## Setup

add laravel-datatables to your composer.json

```
"westwerk/laravel-datatables": "dev-develop",
```

add the repostitory to the composer json since the package is published on packagist

```
"repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:westwerk/laravel-datatables.git"
    }
  ]
```

register in the

## Use Datatables

You need

- unicorns
- luck
- react
- datatables

Example DataTableRequest Class

```
<?php

namespace DatatablesDemo\Http\Requests\Api\DataTables;

use Illuminate\Database\Eloquent\Builder;
use Westwerk\DataTables\Http\Requests\DataTableRequest;

/**
 * Class Request
 *
 * @package Takeback\Http\Requests\Api\DataTables
 */
class DemoDataTableRequest extends DataTableRequest
{

    /**
     * @return array
     */
    public function setupColumns()
    {
        return [
            [
                'props' => [
                    'title' => '#',
                    'sortable' => true,
                    'filter' => [
                        'type' => 'text',
                    ]
                ],
                'filter' => 'id',
                'sort' => 'id',
                'value' => 'id'
            ],
            [
                'props' => [
                    'title' => 'Identifier',
                    'sortable' => true,
                    'filter' => [
                        'type' => 'text',
                    ]
                ],
                'filter' => 'number',
                'sort' => 'number',
                'value' => 'number'
            ],
            [
                'props' => [
                    'title' => 'Name',
                    'sortable' => true,
                    'filter' => [
                        'type' => 'text',
                    ]
                ],
                'filter' => 'name',
                'sort' => 'name',
                'value' => 'name'
            ],
            [
                'props' => [
                    'title' => 'Created at',
                    'sortable' => true,
                    'filter' => [
                        'type' => 'daterange',
                        'props' => [
                        ]
                    ]
                ],
                'filter' => 'created_at',
                'sort' => 'created_at',
                'value' => function ($row) {
                    return ($row->created_at ? $row->created_at->format('d.m.Y') : '');
                }
            ],
            [
                'props' => [
                    'title' => '',
                    'sortable' => false,
                ],
                'exportable' => false,
                'value' => function (Company $user) {
                    return view('datatables.company.actions', ['model' => $user]);
                }
            ],
        ];
    }

    /**
     * @param Builder $qb
     */
    public function setupQueryBuilder(Builder $qb)
    {
        $qb->select('*');
    }
}

```

### Columns

- The key of your column is written into `$props[alias]`.
- `$value` does accept values of different type:
  - `string`: `$value` is used as property name and translated: `trans($row->$value)`
  - `callable`: the whole row/entity is passed to the callable and its return value is used
  - `array`: the first array item is used to retrieve the value, all other items are modifiers
    - Example: `['id', 'intval', 'foobar']` would equal `foobar(intval($row->id))`
    - The first item can be a callable (like above)
    - The other items can be callables, too, but only receive the value instead of the row:
      ```php
      [function ($row) { // get value from row/entity
          return $row->id;
      }, function ($value) { // modify value
          return intval($value);
      }]
      ```

```
'myColumn' => [
    'props' => [
        'title' => 'Column title',
        'sortable' => true, // Enable / Disable Sorting
        'filter' => [
            'type' => 'text', // See filter
        ]
    ],
    'exportable' => false, // Is the column exportable?
    'filter' => function (Builder $query, $column, $value, $global) {
        $query->where(
            \DB::raw,
            'like',
            '%' . $value . '%'
        );
    },
    'sort' => 'myColumn',
    'value' => 'myColumn'
],
```

You can also return a blade view instead of just a value:

```
'value' => function ($row) {
    return view('datatables.myView.myColumn', ['model' => $row]);
}
```

Need to filter over more than one field? There you go. But keep in mind to setup your query-builder accordingly.

```
'address' => [
    'props' => [
        'title' => 'Address',
        'exportable' => false,
        'filter' => [
            'type' => 'text'
        ],
    ],
    'exportable' => false,
    'filter' => function (Builder $query, $column, $value, $global) {
        $values = explode(' ', $value);

        $columns = [
            'locations.number',
            'locations.contact',
            'locations.info',
            'addresses.contact',
            'addresses.organization',
            'addresses.unit',
            'addresses.street',
            'addresses.zip',
            'addresses.district',
            'addresses.city',
            'addresses.phone',
            'addresses.email',
            'addresses.building',
            'regions.iso_code'
        ];

        foreach ($values as $v) {
            $query = $query->where(function ($query) use ($columns, $v) {
                foreach ($columns as $c) {
                    $query = $query->orWhere($c, 'like', '%' . $v . '%');
                }
            });
        }
    },
    'value' => function ($row) {
        return view('datatables.order.address', ['model' => $row]);
    }
],
```

### Filter

Valid filtertypes are

- date
- daterange
- bool
- select

### Query Builder

```
/**
* @param Builder $qb
*/
public function setupQueryBuilder(Builder $qb)
{
$qb
    ->join('services', 'orders.service_id', '=', 'services.id')
    ->join('locations', 'orders.location_id', '=', 'locations.id')
    ->join('addresses', 'locations.id', '=', 'addresses.addressable_id')
    ->join('regions', 'addresses.region_id', '=', 'regions.id')
    ->select(
        'orders.*',
        \DB::raw('UPPER(CONCAT(tenants.code_part,\'-\',orders.code_part)) AS order_code')
    )
}
```
