## 1.0.6
* merge last two versions which for some reason were conflicting
* remove unused abstract method `tableQueryBuilder` from request

## 1.0.5
* migrated to PhpSpreadsheet as PhpExcel does not support PHP >= 7.1

## 1.0.4
* fixed bug where input `length` == -1 resulted in an error
  * instead do not limit number of results

## 1.0.3
* skipped version

## 1.0.2
* fixed bug with laravel 5.4

## 1.0.1
* fixed bug where column value callback was broken
* fixed bug where column value view was not rendered

## 1.0.0
* Initial release